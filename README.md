#CurrencyConverter

### Requirements

The project was bult using the following tools/configurations: 

* Xcode: `10.1`
* Swift: `4.2`
* Cocoapods: `1.5.3`
* Minimum deployment version: `iOS 12.0`

### Build instructions

* Open **CurrencyConverter.xcworkspace**
* **Run** the project.

**Note:** This project uses Cocoapods for dependency manager, although, to avoid issues during review, all dependency files were included in version control. Running `pod install` is **NOT REQUIRED**.


### Dependencies

* [Kingfisher (4.10.1)](https://github.com/onevcat/Kingfisher): used for downloading and caching images from the web

### Additional notes

* Flag Image files are downloaded from this repo: https://github.com/transferwise/currency-flags

* The file used to prepopulate currency data (`CurrencyStoreSeed.json`) was adapted from here: https://github.com/IainMcManus/FlagAndCountryData/blob/master/Currencies.json
