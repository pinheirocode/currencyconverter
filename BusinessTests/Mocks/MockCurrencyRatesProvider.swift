//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation
import Model

class MockCurrencyRatesProvider: CurrencyRatesProvider {

  var invokedFetchCurrencyRates = false
  var invokedFetchCurrencyRatesCount = 0
  var invokedFetchCurrencyRatesParameters: (baseCurrency: String, Void)?
  var invokedFetchCurrencyRatesParametersList = [(baseCurrency: String, Void)]()
  var stubbedFetchCurrencyRatesResultHandlerResult: (Result<CurrencyRatesResponse>, Void)?
  var stubbedFetchCurrencyRatesResult: CancelableTask!

  func fetchCurrencyRates(baseCurrency: String, resultHandler: @escaping FetchCurrencyRatesResultHandler) -> CancelableTask? {
    invokedFetchCurrencyRates = true
    invokedFetchCurrencyRatesCount += 1
    invokedFetchCurrencyRatesParameters = (baseCurrency, ())
    invokedFetchCurrencyRatesParametersList.append((baseCurrency, ()))
    if let result = stubbedFetchCurrencyRatesResultHandlerResult {
      resultHandler(result.0)
    }
    return stubbedFetchCurrencyRatesResult
  }
}
