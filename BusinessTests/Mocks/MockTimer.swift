//
// Created by Bruno Pinheiro on 2018-11-13.
//

import Foundation
@testable import Business

class MockTimer: RepeatableTimer {

  var invokedEventHandlerSetter = false
  var invokedEventHandlerSetterCount = 0
  var invokedEventHandler: (() -> ())?
  var invokedEventHandlerList = [(() -> ())?]()
  var invokedEventHandlerGetter = false
  var invokedEventHandlerGetterCount = 0
  var stubbedEventHandler: (() -> ())!
  var eventHandler: (() -> ())? {
    set {
      invokedEventHandlerSetter = true
      invokedEventHandlerSetterCount += 1
      invokedEventHandler = newValue
      invokedEventHandlerList.append(newValue)
    }
    get {
      invokedEventHandlerGetter = true
      invokedEventHandlerGetterCount += 1
      return stubbedEventHandler
    }
  }
  var invokedResume = false
  var invokedResumeCount = 0

  func resume() {
    invokedResume = true
    invokedResumeCount += 1
  }

  var invokedSuspend = false
  var invokedSuspendCount = 0

  func suspend() {
    invokedSuspend = true
    invokedSuspendCount += 1
  }
}
