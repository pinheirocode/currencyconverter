//
// Created by Bruno Pinheiro on 2018-11-12.
//

import Foundation
import Model

class MockCancelableTask: CancelableTask {
  var cancelCalled = false
  func cancel() {
    cancelCalled = true
  }
}
