//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation
import Model

class MockCurrencyStore: CurrencyDataStore {

  required init(seed: [[String: String]]) {
  }

  var invokedCurrencyCodesForKnownRatesGetter = false
  var invokedCurrencyCodesForKnownRatesGetterCount = 0
  var stubbedCurrencyCodesForKnownRates: [String]! = []
  var currencyCodesForKnownRates: [String] {
    invokedCurrencyCodesForKnownRatesGetter = true
    invokedCurrencyCodesForKnownRatesGetterCount += 1
    return stubbedCurrencyCodesForKnownRates
  }
  var invokedCurrencyRatesGetter = false
  var invokedCurrencyRatesGetterCount = 0
  var stubbedCurrencyRates: [String: Double]! = [:]
  var currencyRates: [String: Double] {
    invokedCurrencyRatesGetter = true
    invokedCurrencyRatesGetterCount += 1
    return stubbedCurrencyRates
  }
  var invokedName = false
  var invokedNameCount = 0
  var invokedNameParameters: (code: String, Void)?
  var invokedNameParametersList = [(code: String, Void)]()
  var stubbedNameResult: String!

  func name(forCurrencyCode code: String) -> String? {
    invokedName = true
    invokedNameCount += 1
    invokedNameParameters = (code, ())
    invokedNameParametersList.append((code, ()))
    return stubbedNameResult
  }

  var invokedFlagUrl = false
  var invokedFlagUrlCount = 0
  var invokedFlagUrlParameters: (code: String, Void)?
  var invokedFlagUrlParametersList = [(code: String, Void)]()
  var stubbedFlagUrlResult: String!

  func flagUrl(forCurrencyCode code: String) -> String? {
    invokedFlagUrl = true
    invokedFlagUrlCount += 1
    invokedFlagUrlParameters = (code, ())
    invokedFlagUrlParametersList.append((code, ()))
    return stubbedFlagUrlResult
  }

  var invokedUpdate = false
  var invokedUpdateCount = 0
  var invokedUpdateParameters: (currencyRates: [String: Double], Void)?
  var invokedUpdateParametersList = [(currencyRates: [String: Double], Void)]()

  func update(currencyRates: [String: Double]) {
    invokedUpdate = true
    invokedUpdateCount += 1
    invokedUpdateParameters = (currencyRates, ())
    invokedUpdateParametersList.append((currencyRates, ()))
  }

  var invokedRate = false
  var invokedRateCount = 0
  var invokedRateParameters: (code: String, Void)?
  var invokedRateParametersList = [(code: String, Void)]()
  var stubbedRateResult: Double!

  func rate(forCurrencyCode code: String) -> Double? {
    invokedRate = true
    invokedRateCount += 1
    invokedRateParameters = (code, ())
    invokedRateParametersList.append((code, ()))
    return stubbedRateResult
  }
}
