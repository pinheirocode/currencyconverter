//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation
import XCTest
import Model
@testable import Business

final class CurrencyRatesViewModelTests: XCTestCase {

  let mockCurrencyCode = "MK1"
  let mockCurrencyName = "Mock Currency 1"
  let mockFlagUrl = "https://mock.url/flags/mk1.png"
  let mockDate = "01-01-2000"

  let mockCurrencyInfo: [[String: String]] = [
    [
      "code": "MK1",
      "flagUrl": "https://mock.url/flags/mk1.png",
      "name": "Mock Currency 1"
    ],
    [
      "code": "MK2",
      "flagUrl": "https://mock.url/flags/mk2.png",
      "name": "Mock Currency 2"
    ],
    [
      "code": "MK3",
      "flagUrl": "https://mock.url/flags/mk3.png",
      "name": "Mock Currency 3"
    ]
  ]

  let mockRates = [
    "MK1": 1.9876,
    "MK2": 12.3456,
    "MK3": 0.3456
  ]

  var mockCurrencyStore: MockCurrencyStore!
  var mockRatesProvider: MockCurrencyRatesProvider!

  override func setUp() {
    super.setUp()
    mockCurrencyStore = MockCurrencyStore(seed: mockCurrencyInfo)
    mockRatesProvider = MockCurrencyRatesProvider()
  }

  func testUpdateCurrencyData_FetchesCurrencyFromProvider() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)

    // When
    viewModel.updateCurrencyRates { _ in /*Ignored*/ }

    // Then
    XCTAssertTrue(mockRatesProvider.invokedFetchCurrencyRates)
  }

  func testUpdateCurrencyData_PassesBaseCurrencyCode() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockCurrencyCode = "MMM"
    viewModel.baseCurrency = mockCurrencyCode

    // When
    viewModel.updateCurrencyRates { _ in /*Ignored*/ }

    // Then
    XCTAssertEqual(mockRatesProvider.invokedFetchCurrencyRatesParameters?.baseCurrency, mockCurrencyCode)
  }

  func testUpdateCurrencyData_SetsOngoingUpdateToken() {
    // Given
    mockRatesProvider.stubbedFetchCurrencyRatesResult = MockCancelableTask()
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)

    // When
    viewModel.updateCurrencyRates { _ in /*Ignored*/ }

    // Then
    XCTAssertNotNil(viewModel.ongoingUpdate)
  }

  func testUpdateCurrencyData_Success_ClearsOngoingUpdateToken() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockResponse = CurrencyRatesResponse(base: "MCK", date: "01-01-1990", rates: [:])

    // When
    let stubbedResult = (Result<CurrencyRatesResponse>.success(mockResponse), ())
    mockRatesProvider.stubbedFetchCurrencyRatesResultHandlerResult = stubbedResult
    viewModel.updateCurrencyRates { _ in /*Ignored*/ }

    // Then
    XCTAssertNil(viewModel.ongoingUpdate)
  }

  func testUpdateCurrencyData_Success_UpdatesObjectStore_IncludingBaseWithRate_1() {
    // Given
    prepareSuccessRatesUpdate()
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)

    // When
    let callbackExecuted = expectation(description: "callbackExecuted")
    viewModel.updateCurrencyRates { _ in
      callbackExecuted.fulfill()
    }

    // Then
    var expectedRates = mockRates
    expectedRates[mockCurrencyCode] = 1
    waitForExpectations(timeout: 1) { _ in
      XCTAssertEqual(self.mockCurrencyStore.invokedUpdateParameters?.currencyRates, expectedRates)
    }
  }

  func testStartPolling_SetsTimerHolderProperty() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockTimer = MockTimer()

    // When
    viewModel.startPollingCurrencyData(usingTimer: mockTimer)

    // Then
    XCTAssertNotNil(viewModel.pollingTimer)
  }

  func testStopPolling_ClearsTimerHolderProperty() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockTimer = MockTimer()
    viewModel.startPollingCurrencyData(usingTimer: mockTimer)

    // When
    viewModel.stopPollingCurrencyData()

    // Then
    XCTAssertNil(viewModel.pollingTimer)
  }

  func testStartPolling_SetsTimerEventHandler() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockTimer = MockTimer()

    // When
    viewModel.startPollingCurrencyData(usingTimer: mockTimer)

    // Then
    XCTAssertTrue(mockTimer.invokedEventHandlerSetter)
  }

  func testPolling_TimerTick_FetchesCurrencyFromProvider() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockTimer = MockTimer()

    // When
    viewModel.startPollingCurrencyData(usingTimer: mockTimer)
    mockTimer.invokedEventHandler?()
    mockTimer.invokedEventHandler?()

    // Then
    XCTAssertEqual(mockRatesProvider.invokedFetchCurrencyRatesCount, 2)
  }

  func testPolling_TimerTick_SetsOngoingUpdate() {
    // Given
    mockRatesProvider.stubbedFetchCurrencyRatesResult = MockCancelableTask()
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockTimer = MockTimer()
    viewModel.startPollingCurrencyData(usingTimer: mockTimer)

    // When
    mockTimer.invokedEventHandler?()

    // Then
    XCTAssertNotNil(viewModel.ongoingUpdate)
  }

  func testPolling_UpdateSuccess_ClearsOngoingUpdateToken() {
    // Given
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockResponse = CurrencyRatesResponse(base: "MCK", date: "01-01-1990", rates: [:])
    let mockTimer = MockTimer()
    viewModel.startPollingCurrencyData(usingTimer: mockTimer)

    // When
    let stubbedResult = (Result<CurrencyRatesResponse>.success(mockResponse), ())
    mockRatesProvider.stubbedFetchCurrencyRatesResultHandlerResult = stubbedResult
    mockTimer.invokedEventHandler?()

    // Then
    XCTAssertNil(viewModel.ongoingUpdate)
  }

  func testPolling_UpdateSuccess_UpdatesObjectStore() {
    // Given
    prepareSuccessRatesUpdate()
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: mockCurrencyStore)
    let mockTimer = MockTimer()

    // When
    expectation(for: NSPredicate(block: { object, _ in
      return (object as! MockCurrencyStore).invokedUpdate
    }), evaluatedWith: mockCurrencyStore)
    viewModel.startPollingCurrencyData(usingTimer: mockTimer)
    mockTimer.invokedEventHandler?()

    // Then
    var expectedRates = mockRates
    expectedRates[mockCurrencyCode] = 1
    waitForExpectations(timeout: 5) { _ in
      XCTAssertEqual(self.mockCurrencyStore.invokedUpdateParameters?.currencyRates, expectedRates)
    }
  }

  func testPrepareCurrenciesData_BaseCurrencyIsFirstCurrency() {
    // Given
    let currency1 = "MK1"
    let currency2 = "MK2"
    let store = CurrencyStore(seed: mockCurrencyInfo)
    store.update(currencyRates: [currency1: 1, currency2: 0.5])
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: store)
    viewModel.currentAmount = 10.0
    viewModel.baseCurrency = currency1

    // When
    let beforeUpdate = viewModel.prepareCurrenciesData().first
    viewModel.baseCurrency = currency2
    let afterUpdate = viewModel.prepareCurrenciesData().first

    // Then
    XCTAssertEqual(beforeUpdate?.code, currency1)
    XCTAssertEqual(afterUpdate?.code, currency2)
  }

  func testConvertCurrency_ReturnsConvertedAmount() {
    // Given
    let originCurrency = "ORG"
    let targetCurrency = "TGT"
    let store = CurrencyStore(seed: mockCurrencyInfo)
    store.update(currencyRates: [originCurrency: 2.5, targetCurrency: 0.5])
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: store)

    // When
    let converted = viewModel.convert(amount: 50.0, from: originCurrency, to: targetCurrency)

    // Then
    let expectedValue = 10.0 // (mockAmount / originCurrencyRate) * targetCurrencyRate
    XCTAssertEqual(converted, expectedValue)
  }

  func testCurrencyRatesCount_ReturnsCorrectValue() {
    // Given
    let viewModel = prepareViewModelWithDefaultStore()

    // When
    let count = viewModel.currencyRatesCount

    // Then
    XCTAssertEqual(count, mockRates.count)
  }

  func testSetBaseCurrencyAtIndexPath_MovesNewBaseCurrencyToTopOfTheList() {
    // Given
    let viewModel = prepareViewModelWithDefaultStore()
    let firstIndex = IndexPath(row: 0, section: 0)
    viewModel.baseCurrency = viewModel.currency(at: firstIndex).code

    // When
    let newBaseIndex = IndexPath(row: 1, section: 0)
    viewModel.setBase(currencyAt: newBaseIndex)

    // Then
    XCTAssertEqual(viewModel.baseCurrency, viewModel.currency(at: firstIndex).code)
  }

  func testCurrentAmount_DefaultValueIs_1() {
    // Given
    let viewModel = prepareViewModelWithDefaultStore()

    // When
    let amount = viewModel.currentAmount

    // Then
    XCTAssertEqual(amount, 1.0)
  }

  func testUpdateCurrentAmount_UpdatesAllCurrenciesData() {
    // Given
    let viewModel = prepareViewModelWithDefaultStore()

    // When
    let beforeUpdate = viewModel.currencies.map { currency -> (String, String?) in
      return (currency.code, currency.amount)
    }
    viewModel.currentAmount = 10

    // Then
    let afterUpdate = viewModel.currencies.map { currency -> (String, String?) in
      return (currency.code, currency.amount)
    }

    let allUpdated = zip(beforeUpdate, afterUpdate).reduce(true) { (result, tuple) -> Bool in
      let (before, after) = tuple
      let samePosition = before.0 == after.0
      let differentAmounts = before.1 != after.1
      return result && samePosition && differentAmounts
    }
    XCTAssertTrue(allUpdated)
  }

}

private extension CurrencyRatesViewModelTests {
  func prepareSuccessRatesUpdate() {
    let mockRatesResponse = CurrencyRatesResponse(base: mockCurrencyCode, date: mockDate, rates: mockRates)
    mockRatesProvider.stubbedFetchCurrencyRatesResultHandlerResult = (.success(mockRatesResponse), ())
  }

  func prepareViewModelWithDefaultStore() -> CurrencyRatesViewModel {
    let store = CurrencyStore(seed: mockCurrencyInfo)
    store.update(currencyRates: mockRates)
    let viewModel = CurrencyRatesViewModel(currencyRatesProvider: mockRatesProvider, currencyStore: store)
    return viewModel
  }
}

private extension CurrencyRatesViewModel {
  convenience init(currencyRatesProvider: CurrencyRatesProvider, currencyStore: CurrencyDataStore) {
    self.init(baseCurrency: "MK1", currencyRatesProvider: currencyRatesProvider, currencyStore: currencyStore)
  }
}
