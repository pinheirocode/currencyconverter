//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

class MockDownloadDataTask: URLSessionDownloadTask {
  var resultFileUrl: URL?
  var urlResponse: URLResponse?
  var responseError: Error?
  var completionHandler: (URL?, URLResponse?, Error?) -> Void
  var executed = false

  init(
    resultFileUrl: URL? = nil,
    urlResponse: URLResponse? = MockHTTPResponse(),
    responseError: Error? = nil,
    handler: @escaping (URL?, URLResponse?, Error?) -> Void = { _, _, _ in return }) {

    self.resultFileUrl = resultFileUrl
    self.urlResponse = urlResponse
    self.responseError = responseError
    self.completionHandler = handler

    super.init()
  }

  override func resume() {
    executed = true
    completionHandler(resultFileUrl, urlResponse, responseError)
  }

  override func cancel() {}
}
