//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

class MockDataTask: URLSessionDataTask {
  var responseData: Data?
  var urlResponse: URLResponse?
  var responseError: Error?
  var completionHandler: (Data?, URLResponse?, Error?) -> Void
  var executed = false

  init(
    responseData: Data? = nil,
    urlResponse: URLResponse? = MockHTTPResponse(),
    responseError: Error? = nil,
    handler: @escaping (Data?, URLResponse?, Error?) -> Void = { _, _, _ in return }) {

    self.responseData = responseData
    self.urlResponse = urlResponse
    self.responseError = responseError
    self.completionHandler = handler

    super.init()
  }

  override func resume() {
    executed = true
    completionHandler(responseData, urlResponse, responseError)
  }

  override func cancel() {}
}

extension MockDataTask {
  class var mockSuccessPost: MockDataTask {
    return MockDataTask(urlResponse: MockHTTPResponse(status: 201))
  }
  class var mockSuccessGet: MockDataTask {
    return MockDataTask(urlResponse: MockHTTPResponse(status: 200))
  }
}
