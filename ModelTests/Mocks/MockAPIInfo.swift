//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation
import Model

struct MockAPIInfo: APIInfo {
  var currencyRatesUrl: String
}
