//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

class MockUrlSession: URLSession {
  var requestUrl: String?
  var mockDataTask = MockDataTask()
  var receivedRequest: URLRequest?
  var responseData: Data?

  var mockDownloadDataTask = MockDownloadDataTask()
  var receivedDownloadRequest: URLRequest?
  var fileUrl: URL?

  override init() {
  }

  override func dataTask(with request: URLRequest) -> URLSessionDataTask {
    receivedRequest = request
    return MockDataTask { _, _, _ in return }
  }

  override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
    receivedRequest = request
    mockDataTask.completionHandler = completionHandler
    return mockDataTask
  }

  override func dataTask(with url: URL) -> URLSessionDataTask {
    fatalError()
  }

  override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
    fatalError()
  }

  override func downloadTask(with request: URLRequest) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(with url: URL) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(withResumeData resumeData: Data) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(with request: URLRequest, completionHandler: @escaping (URL?, URLResponse?, Error?) -> Void) -> URLSessionDownloadTask {
    receivedDownloadRequest = request
    mockDownloadDataTask.completionHandler = completionHandler
    return mockDownloadDataTask
  }

  override func downloadTask(with url: URL, completionHandler: @escaping (URL?, URLResponse?, Error?) -> Void) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(withResumeData resumeData: Data, completionHandler: @escaping (URL?, URLResponse?, Error?) -> Void) -> URLSessionDownloadTask {
    fatalError()
  }
}