//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation
import XCTest
import TestStubs
@testable import Model

final class CurrencyRatesServiceTests: XCTestCase {
  private let mockCurrencyRatesUrl = "http://mock.base.url/currency/rates?base=EUR"
  private let mockCurrency = "MCK"

  private var mockAPIInfo: MockAPIInfo!
  private var mockSession: MockUrlSession!

  override func setUp() {
    super.setUp()
    mockAPIInfo = MockAPIInfo(currencyRatesUrl: mockCurrencyRatesUrl)
    mockSession = MockUrlSession()
  }

  override func tearDown() {
    mockAPIInfo = nil
    mockSession = nil
    super.tearDown()
  }

  func testFetchCurrencyRates_CreatesRequestWithHttpMethod_GET() {
    // Given
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { _ in /*do nothing*/ }

    // Then
    XCTAssertEqual(self.mockSession.receivedRequest?.httpMethod, "GET")
  }

  func testFetchCurrencyRates_SetsBaseCurrencyQueryParam() {
    // Given
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { _ in /*do nothing*/ }

    // Then
    if let url = self.mockSession.receivedRequest?.url {
      XCTAssertTrue(url.absoluteString.hasSuffix("base=\(mockCurrency)"))
    } else {
      XCTFail("Failed to get request url")
    }
  }

  func testFetchCurrencyRates_SendsTheRequest() {
    // Given
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { _ in /*do nothing*/ }

    // Then
    XCTAssertTrue(self.mockSession.mockDataTask.executed)
  }

  // MARK: - Response Handling

  func testFetchCurrencyRates_Success_ParsesResponse_AndCallsCompletionWithSuccessResult() {
    // Given
    let mockRatesResponse = Stubs.load(CurrencyRatesResponse.self)
    let mockData = try! JSONEncoder().encode(mockRatesResponse)
    mockSession.mockDataTask = MockDataTask(responseData: mockData, urlResponse: MockHTTPResponse(status: 200))
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    let responseReceived = expectation(description: "handler called")

    var ratesResponse: CurrencyRatesResponse? = nil
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { result in
      responseReceived.fulfill()
      guard case .success(let object) = result else { return }
      ratesResponse = object
    }

    // Then
    waitForExpectations(timeout: 1) { error in
      XCTAssertEqual(ratesResponse, mockRatesResponse)
    }
  }

  // MARK: - Failure

  func testFetchCurrencyRates_InvalidUrl_CallsHandlerWithFailure_Error_InvalidUrl() {
    // Given
    let invalidUrlFormat = "invalid url format !"
    let mockAPIInfo = MockAPIInfo(currencyRatesUrl: invalidUrlFormat)
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    let handlerCalled = expectation(description: "handler called")
    var error: ProviderError?
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { result in
      handlerCalled.fulfill()
      guard case .failure(let receivedError) = result else { return }
      error = receivedError
    }

    // Then
    waitForExpectations(timeout: 1) { _ in
      XCTAssertEqual(error, ProviderError.invalidUrlFormat)
    }
  }

  func testFetchCurrencyRates_SuccessResponse_MissingResponseData_CallsFailureWithEmptyResponseError() {
    // Given
    mockSession.mockDataTask = .mockSuccessGet
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    let handlerCalled = expectation(description: "handler called")
    var error: ProviderError?
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { result in
      handlerCalled.fulfill()
      guard case .failure(let receivedError) = result else { return }
      error = receivedError
    }

    // Then
    waitForExpectations(timeout: 1) { _ in
      XCTAssertEqual(error, ProviderError.emptyResponse)
    }
  }

  func testFetchCurrencyRates_ErrorReceived_CallsFailureWithRequestFailed() {
    struct MockError: Error {
    }

    // Given
    mockSession.mockDataTask = MockDataTask(responseError: MockError())
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    let handlerCalled = expectation(description: "handler called")
    var error: ProviderError?
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { result in
      handlerCalled.fulfill()
      guard case .failure(let receivedError) = result else { return }
      error = receivedError
    }

    // Then
    waitForExpectations(timeout: 1) { _ in
      XCTAssertEqual(error, ProviderError.requestFailed)
    }
  }

  func testFetchCurrencyRates_SuccessResponse_MalformedResponseData_CallsFailureWithParsingError() {
    // Given
    mockSession.mockDataTask = .mockSuccessGet
    mockSession.mockDataTask.responseData = "{".data(using: .utf8) // malformed json
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    let handlerCalled = expectation(description: "handler called")
    var error: ProviderError?
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { result in
      handlerCalled.fulfill()
      guard case .failure(let receivedError) = result else { return }
      error = receivedError
    }

    // Then
    waitForExpectations(timeout: 1) { _ in
      XCTAssertEqual(error, ProviderError.parsingError)
    }
  }

  func testFetchCurrencyRates_StatusNot200_CallsHandlerWithFailure_ErrorRequestFailed() {
    // Given
    mockSession.mockDataTask = MockDataTask(urlResponse: MockHTTPResponse(status: 401))
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    let handlerCalled = expectation(description: "handler called")
    var error: ProviderError?
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { result in
      handlerCalled.fulfill()
      guard case .failure(let receivedError) = result else { return }
      error = receivedError
    }

    // Then
    waitForExpectations(timeout: 1) { _ in
      XCTAssertEqual(error, ProviderError.requestFailed)
    }
  }

  func testFetchCurrencyRates_MissingURLResponse_CallsHandlerWithFailure_ErrorUnknown() {
    // Given
    mockSession.mockDataTask = MockDataTask(urlResponse: nil)
    let provider = CurrencyRatesService(apiInfo: mockAPIInfo, urlSession: mockSession)

    // When
    let handlerCalled = expectation(description: "handler called")
    var error: ProviderError?
    _ = provider.fetchCurrencyRates(baseCurrency: mockCurrency) { result in
      handlerCalled.fulfill()
      guard case .failure(let receivedError) = result else { return }
      error = receivedError
    }

    // Then
    waitForExpectations(timeout: 1) { _ in
      XCTAssertEqual(error, ProviderError.unknownError)
    }
  }
}
