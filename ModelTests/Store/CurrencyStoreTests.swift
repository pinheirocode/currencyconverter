//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation
import XCTest
import TestStubs
@testable import Model

final class CurrencyStoreTests: XCTestCase {
  let mockCurrencyInfo = Stubs.load([[String: String]].self, named: "MockCurrencyInfo")

  var currencyStore: CurrencyStore!

  override func setUp() {
    super.setUp()
    currencyStore = CurrencyStore()
  }

  override func tearDown() {
    currencyStore = nil
    super.tearDown()
  }

  func testSeedInfoData_PopulatesCurrencyCodes() {
    // Given
    let mockCurrencyCode1 = "MK1"
    let mockCurrencyCode2 = "MK2"
    let mockCurrencyInfo = [
      ["code": mockCurrencyCode1],
      ["code": mockCurrencyCode2]
    ]

    // When
    currencyStore = CurrencyStore(seed: mockCurrencyInfo)

    // Then
    let expectedCodes = [mockCurrencyCode1, mockCurrencyCode2]
    XCTAssertEqual(currencyStore.currencyCodes, expectedCodes)
  }

  func testSeedInfoData_PopulatesCurrencyNames() {
    // Given
    let mockCurrencyCode = "MCK"
    let mockCurrencyName = "mock currency"
    let mockCurrencyInfo = [[
      "code": mockCurrencyCode,
      "name": mockCurrencyName
    ]]

    // When
    currencyStore = CurrencyStore(seed: mockCurrencyInfo)

    // Then
    XCTAssertEqual(currencyStore.name(forCurrencyCode: mockCurrencyCode), mockCurrencyName)
  }

  func testSeedInfoData_PopulatesCurrencyFlagUrl() {
    // Given
    let mockCurrencyCode = "MCK"
    let mockCurrencyFlagUrl = "https://mock.currency.flag/mck.png"
    let mockCurrencyInfo = [[
      "code": mockCurrencyCode,
      "flagUrl": mockCurrencyFlagUrl
    ]]

    // When
    currencyStore = CurrencyStore(seed: mockCurrencyInfo)

    // Then
    XCTAssertEqual(currencyStore.flagUrl(forCurrencyCode: mockCurrencyCode), mockCurrencyFlagUrl)
  }

  func testUpdateRating_PopulatesCurrencyRating() {
    // Given
    let mockCurrencyCode = "MCK"
    let mockCurrencyRating: Double = 1.62
    let ratings = [
      mockCurrencyCode: mockCurrencyRating
    ]

    // When
    currencyStore.update(currencyRates: ratings)

    // Then
    XCTAssertEqual(currencyStore.rate(forCurrencyCode: mockCurrencyCode), mockCurrencyRating)
  }
}
