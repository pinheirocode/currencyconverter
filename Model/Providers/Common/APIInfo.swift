//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

public protocol APIInfo {
  var currencyRatesUrl: String { get }
}
