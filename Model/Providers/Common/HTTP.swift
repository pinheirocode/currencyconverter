//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

struct HTTPMethod {
  static let get: String = "GET"
  private init() {}
}
