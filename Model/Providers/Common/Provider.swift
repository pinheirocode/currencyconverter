//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation

public protocol CancelableTask {
  func cancel()
}

public enum ProviderError: Error {
  case requestFailed, unknownError, invalidUrlFormat, emptyResponse, parsingError
}

public enum Result<T> {
  case success(T), failure(ProviderError)
}


