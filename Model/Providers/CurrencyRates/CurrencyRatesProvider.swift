//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

public typealias FetchCurrencyRatesResultHandler = (Result<CurrencyRatesResponse>) -> ()

public protocol CurrencyRatesProvider {
  func fetchCurrencyRates(baseCurrency: String, resultHandler: @escaping FetchCurrencyRatesResultHandler) -> CancelableTask?
}
