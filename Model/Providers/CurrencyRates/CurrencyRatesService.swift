//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

public final class CurrencyRatesService: CurrencyRatesProvider {

  private var apiInfo: APIInfo
  private var urlSession: URLSession

  public init(apiInfo: APIInfo, urlSession: URLSession = URLSession(configuration: .default)) {
    self.apiInfo = apiInfo
    self.urlSession = urlSession
  }

  public func fetchCurrencyRates(baseCurrency: String, resultHandler: @escaping FetchCurrencyRatesResultHandler) -> CancelableTask? {
    guard let currencyRatesUrl = URL(string: apiInfo.currencyRatesUrl) else {
      resultHandler(.failure(ProviderError.invalidUrlFormat))
      return nil
    }

    var urlComponents = URLComponents(url: currencyRatesUrl, resolvingAgainstBaseURL: false)

    urlComponents?.query = "base=\(baseCurrency)"

    guard let composedUrl = urlComponents?.url else {
      resultHandler(.failure(ProviderError.unknownError))
      return nil
    }

    var request = URLRequest(url: composedUrl)

    request.httpMethod = HTTPMethod.get

    let dataTask = urlSession.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
      guard error == nil else {
        resultHandler(.failure(ProviderError.requestFailed))
        return
      }

      guard let response = response as? HTTPURLResponse else {
        resultHandler(.failure(ProviderError.unknownError))
        return
      }

      guard response.statusCode == 200 else {
        resultHandler(.failure(ProviderError.requestFailed))
        return
      }

      guard let data = data else {
        resultHandler(.failure(ProviderError.emptyResponse))
        return
      }

      guard let currencyRates = try? JSONDecoder().decode(CurrencyRatesResponse.self, from: data) else {
        resultHandler(.failure(ProviderError.parsingError))
        return
      }

      resultHandler(.success(currencyRates))
    }

    dataTask.resume()

    return dataTask
  }
}

extension URLSessionDataTask: CancelableTask {}
