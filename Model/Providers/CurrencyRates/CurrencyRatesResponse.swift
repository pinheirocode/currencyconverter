//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

public struct CurrencyRatesResponse: Codable {
  public var base: String
  public var date: String
  public var rates: [String: Double]

  public init(base: String, date: String, rates: [String: Double]) {
    self.base = base
    self.date = date
    self.rates = rates
  }
}

extension CurrencyRatesResponse: Hashable {
  public var hashValue: Int {
    var result = base.hashValue
    result = result * 31 + date.hashValue
    result = result * 31 + rates.hashValue
    return result
  }

  public static func ==(lhs: CurrencyRatesResponse, rhs: CurrencyRatesResponse) -> Bool {
    if lhs.base != rhs.base { return false }
    if lhs.date != rhs.date { return false }
    if lhs.rates != rhs.rates { return false }
    return true
  }
}
