//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation

public protocol CurrencyDataStore {
  init(seed: [[String: String]])
  var currencyCodesForKnownRates: [String] { get }
  var currencyRates: [String: Double] { get }
  func name(forCurrencyCode code: String) -> String?
  func flagUrl(forCurrencyCode code: String) -> String?
  func update(currencyRates: [String: Double])
  func rate(forCurrencyCode code: String) -> Double?
}
