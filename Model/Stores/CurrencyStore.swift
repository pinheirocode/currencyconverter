//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation

private struct CurrencyInfoKeys {
  static let code = "code"
  static let flagUrl = "flagUrl"
  static let name = "name"
}

public final class CurrencyStore: CurrencyDataStore {
  public private(set) var currencyCodes: [String] = []
  public var currencyCodesForKnownRates: [String] {
    let knownRates = Set(currencyRates.keys)
    return currencyCodes.filter { knownRates.contains($0) }
  }

  private var currencyNames: [String: String] = [:]
  private var currencyFlagUrls: [String: String] = [:]
  public private(set) var currencyRates: [String: Double] = [:]

  public init(seed: [[String: String]] = []) {
    for item in seed {
      guard let currencyCode = item[CurrencyInfoKeys.code] else {
        continue
      }
      currencyCodes.append(currencyCode)
      currencyNames[currencyCode] = item[CurrencyInfoKeys.name]
      currencyFlagUrls[currencyCode] = item[CurrencyInfoKeys.flagUrl]
    }
  }

  public func name(forCurrencyCode code: String) -> String? {
    return currencyNames[code]
  }

  public func flagUrl(forCurrencyCode code: String) -> String? {
    return currencyFlagUrls[code]
  }

  public func update(currencyRates: [String: Double]) {
    self.currencyRates = currencyRates
  }

  public func rate(forCurrencyCode code: String) -> Double? {
    return currencyRates[code]
  }
}
