//
// Created by Bruno Pinheiro on 2018-11-13.
//

import Foundation

protocol RepeatableTimer {
  var eventHandler: (() -> ())? { get set }
  func resume()
  func suspend()
}
