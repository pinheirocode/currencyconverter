//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation

public typealias UpdateObserver<T> = (T) -> ()
