//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation

public protocol LiveCurrencyPresentationDelegate {
  func currencyPresentationDidUpdate(_ currencyPresentation: LiveCurrencyPresentation)
}

public protocol LiveCurrencyPresentation: class {
  var code: String { get }
  var name: String { get }
  var flagUrl: URL? { get }
  var amount: String? { get set }

  var delegate: LiveCurrencyPresentationDelegate? { get set }
}

