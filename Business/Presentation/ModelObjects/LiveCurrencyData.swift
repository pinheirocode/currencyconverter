//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation

class LiveCurrencyData: LiveCurrencyPresentation {
  private(set) var code: String = ""
  private(set) var name: String = ""
  private(set) var flagUrl: URL? = nil
  var amount: String? = nil {
    didSet {
      delegate?.currencyPresentationDidUpdate(self)
    }
  }
  var delegate: LiveCurrencyPresentationDelegate? = nil

  init(code: String, name: String, flagUrl: URL?, amount: String?) {
    self.code = code
    self.name = name
    self.flagUrl = flagUrl
    self.amount = amount
  }
}

extension LiveCurrencyData: Hashable {
  public var hashValue: Int {
    return code.hashValue
  }

  public static func ==(lhs: LiveCurrencyData, rhs: LiveCurrencyData) -> Bool {
    if lhs.code != rhs.code { return false }
    return true
  }
}

extension LiveCurrencyData: CustomDebugStringConvertible {
  public var debugDescription: String {
    return "LiveCurrencyData(code: \(code), name: \(name), flagUrl: \(String(describing:flagUrl)), amount: \(String(describing:amount))"
  }
}
