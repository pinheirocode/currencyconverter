//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation
import Model

public typealias EventHandler<EventType> = (EventType) -> ()

public final class CurrencyRatesViewModel {

  private var currencyRatesProvider: CurrencyRatesProvider
  private var currencyStore: CurrencyDataStore

  var pollingTimer: RepeatableTimer?
  var ongoingUpdate: CancelableTask? {
    didSet {
      DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = self.ongoingUpdate != nil
      }
    }
  }

  var currenciesByCode: [String: LiveCurrencyData] = [:]
  var currencies: [LiveCurrencyData] = [] {
    didSet {
      updateCurrenciesByCode(with: currencies)
    }
  }

  private func updateCurrenciesByCode(with: [LiveCurrencyData]) {
    currenciesByCode.removeAll()
    for item in currencies {
      currenciesByCode[item.code] = item
    }
  }

  public func startPollingCurrencyData() {
    startPollingCurrencyData(usingTimer: RepeatingTimer(timeInterval: 1))
  }

  func startPollingCurrencyData(usingTimer timer: RepeatableTimer) {
    guard pollingTimer == nil else { return }

    pollingTimer = timer

    pollingTimer?.eventHandler = { [weak self] in
      guard let self = self else { return }
      guard self.ongoingUpdate == nil else { return }
      self.ongoingUpdate = self.currencyRatesProvider.fetchCurrencyRates(baseCurrency: self.baseCurrency,
        resultHandler: { [weak self] result in
          print("===>>> Polling update received: \(result)")

          guard let self = self else { return }

          self.ongoingUpdate = nil
          guard case let .success(response) = result else { return }

          self.updateStoreWithResponse(response: response)
          DispatchQueue.main.async { [weak self] in
            self?.updateCurrenciesData()
          }
        })
    }

    pollingTimer?.resume()
  }

  public func stopPollingCurrencyData() {
    pollingTimer?.suspend()
    pollingTimer = nil
  }

  public var currencyRatesCount: Int {
    return currencies.count
  }

  public var currentAmount: Double = 1 {
    didSet { updateCurrenciesData() }
  }

  public var baseCurrency: String {
    didSet {
      guard baseCurrency != oldValue else { return }
      currentAmount = convert(amount: currentAmount, from: oldValue, to: baseCurrency)
    }
  }

  public init(baseCurrency: String, currencyRatesProvider: CurrencyRatesProvider, currencyStore: CurrencyDataStore) {
    self.currencyRatesProvider = currencyRatesProvider
    self.currencyStore = currencyStore
    self.baseCurrency = baseCurrency
    self.currencies = self.prepareCurrenciesData()
  }

  public func updateCurrencyRates(resultHandler: @escaping EventHandler<[LiveCurrencyPresentation]>) {
    self.ongoingUpdate = currencyRatesProvider.fetchCurrencyRates(baseCurrency: self.baseCurrency,
      resultHandler: { [weak self] result in
        self?.ongoingUpdate = nil

        guard case let .success(response) = result else {
          // TODO: Properly notify error
          resultHandler([])
          return
        }

        guard let self = self else { return }

        self.updateStoreWithResponse(response: response)

        if self.currencies.count < self.currencyStore.currencyCodesForKnownRates.count {
          let currencies = self.prepareCurrenciesData()
          self.currencies = currencies
        } else {
          self.updateCurrenciesData()
        }

        resultHandler(self.currencies)
      })
  }

  private func updateStoreWithResponse(response: CurrencyRatesResponse) {
    var rates = response.rates
    rates[response.base] = 1

    currencyStore.update(currencyRates: rates)
  }

  public func currency(at index: IndexPath) -> LiveCurrencyPresentation {
    return currencies[index.row]
  }

  public func setBase(currencyAt indexPath: IndexPath) {
    let newBase = currencies[indexPath.row]
    for index in stride(from: indexPath.row, to: 0, by: -1) {
      currencies[index] = currencies[index - 1]
    }
    currencies[0] = newBase
    self.baseCurrency = newBase.code
  }

  public func updateAmount(withValueString valueString: String?) {
    guard let valueString = valueString,
          let newValue = currencyInputAmountFormatter.number(from: valueString) else {
      currentAmount = 0
      return
    }
    currentAmount = newValue.doubleValue
  }

  func prepareCurrenciesData() -> [LiveCurrencyData] {
    var result = [prepareCurrencyData(code: baseCurrency)]
    let remainingRates: [String] = self.currencyStore.currencyCodesForKnownRates.filter { $0 != self.baseCurrency }

    for code in remainingRates {
      let currencyData = prepareCurrencyData(code: code)
      result.append(currencyData)
    }

    return result
  }

  func updateCurrenciesData() {
    for currency in self.currencies {
      let amount = currentAmount(inCurrency: currency.code)
      let formattedAmount = formatted(amount: amount)
      currency.amount = formattedAmount
    }
  }

  private func prepareCurrencyData(code: String) -> LiveCurrencyData {
    let amount = self.currentAmount(inCurrency: code)
    let formattedAmount = self.formatted(amount: amount)

    let name = self.currencyStore.name(forCurrencyCode: code) ?? ""
    let flagUrlString = self.currencyStore.flagUrl(forCurrencyCode: code) ?? ""
    let flagUrl = URL(string: flagUrlString)

    return LiveCurrencyData(code: code, name: name, flagUrl: flagUrl, amount: formattedAmount)
  }

  private func formatted(amount: Double) -> String? {
    return currencyOutputAmountFormatter.string(from: amount as NSNumber)
  }

  private var currencyInputAmountFormatter: NumberFormatter {
    let formatter = NumberFormatter()
    return formatter
  }

  private var currencyOutputAmountFormatter: NumberFormatter {
    let formatter = NumberFormatter()
    formatter.minimumIntegerDigits = 1
    formatter.minimumFractionDigits = 2
    formatter.maximumFractionDigits = 2
    return formatter
  }

  private func currentAmount(inCurrency code: String) -> Double {
    return convert(amount: currentAmount, from: baseCurrency, to: code)
  }

  public func convert(amount: Double, from originCurrencyCode: String, to targetCurrencyCode: String) -> Double {
    guard let originRate = currencyStore.rate(forCurrencyCode: originCurrencyCode),
          let targetRate = currencyStore.rate(forCurrencyCode: targetCurrencyCode) else {
      return 0
    }

    return (amount / originRate) * targetRate
  }
}
