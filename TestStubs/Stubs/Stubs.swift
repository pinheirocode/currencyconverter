//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation

public final class Stubs {
  public static func load<T>(_ type: T.Type) -> T where T: Decodable {
    return load(type, named: String(describing: type))
  }

  public static func load<T>(_ type: T.Type, named: String) -> T where T: Decodable {
    let url = Bundle(for: Stubs.self).url(forResource: named, withExtension: ".json")!
    let data = try! Data(contentsOf: url)
    return try! JSONDecoder().decode(type, from: data)
  }
}
