//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation
import UIKit

// TODO: Make this designable
final class UnderlinedTextField: UITextField {
  override func draw(_ rect: CGRect) {
    super.draw(rect)

    if self.isFirstResponder {
      UIColor(red: 66.0/255.0, green: 107.0/255.0, blue: 242.0/255, alpha: 1).setFill()
    } else {
      UIColor.lightGray.setFill()
    }

    let lineHeight: CGFloat = 2

    let lineRect = CGRect(x: 0, y: rect.maxY - lineHeight, width: rect.width, height: lineHeight)

    UIBezierPath(roundedRect: lineRect, cornerRadius: lineHeight / 2).fill()
  }

  override func resignFirstResponder() -> Bool {
    self.setNeedsDisplay()
    return super.resignFirstResponder()
  }

  override func becomeFirstResponder() -> Bool {
    self.setNeedsDisplay()
    return super.becomeFirstResponder()
  }
}
