//
//  Created by Bruno Pinheiro on 2018-11-10.
//

import UIKit
import Business
import Model

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    guard let currenciesViewController = window?.rootViewController as? CurrencyRatesViewController else {
      fatalError("Root view controller is not \(String(describing: CurrencyRatesViewController.self))")
    }

    let apiInfo = CurrencyAPIInfo()
    let provider = CurrencyRatesService(apiInfo: apiInfo)
    let store = CurrencyStore(seed: createStoreSeed())
    let model =  CurrencyRatesViewModel(baseCurrency: "EUR", currencyRatesProvider: provider, currencyStore: store)
    currenciesViewController.viewModel = model

    return true
  }

  private func createStoreSeed() -> [[String: String]] {
    let url = Bundle(for: type(of: self)).url(forResource: "CurrencyStoreSeed", withExtension: ".json")!
    let data = try! Data(contentsOf: url)
    return try! JSONDecoder().decode([[String: String]].self, from: data)
  }
}
