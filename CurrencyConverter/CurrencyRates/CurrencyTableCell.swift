//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation
import UIKit
import Business
import Kingfisher

protocol CurrencyCellDelegate: class {
  func currencyCell(_ cell: CurrencyTableCell, didChangeValueTo: String?)
}

final class CurrencyTableCell: UITableViewCell {

  var delegate: CurrencyCellDelegate?

  private struct Constants {
    static let flagPlaceholder = UIImage(named: "image-placeholder")
  }

  @IBOutlet private weak var currencyFlagImageView: UIImageView!
  @IBOutlet private weak var currencyCodeLabel: UILabel!
  @IBOutlet private weak var currencyNameLabel: UILabel!
  @IBOutlet private weak var amountTextField: UITextField!

  override func awakeFromNib() {
    super.awakeFromNib()
    amountTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    currencyFlagImageView.layer.cornerRadius = currencyFlagImageView.frame.height / 2
  }

  var currencyData: LiveCurrencyPresentation? {
    didSet {
      oldValue?.delegate = nil
      update(with: currencyData)
      currencyData?.delegate = self
    }
  }

  private func update(with currency: LiveCurrencyPresentation?) {
    currencyCodeLabel.text = currency?.code
    currencyNameLabel.text = currency?.name
    amountTextField.text = currency?.amount
    currencyFlagImageView.kf.setImage(with: currency?.flagUrl, placeholder: Constants.flagPlaceholder)
  }

  override func becomeFirstResponder() -> Bool {
    amountTextField.isUserInteractionEnabled = true
    return amountTextField.becomeFirstResponder()
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    currencyData = nil
  }

  @objc func textFieldDidChange(_ sender: UITextField) {
    delegate?.currencyCell(self, didChangeValueTo: sender.text)
  }
}

extension CurrencyTableCell: UITextFieldDelegate {
  public func textFieldDidEndEditing(_ textField: UITextField) {
    textField.isUserInteractionEnabled = false
  }
}

extension CurrencyTableCell: LiveCurrencyPresentationDelegate {
  public func currencyPresentationDidUpdate(_ currencyPresentation: LiveCurrencyPresentation) {
    guard !amountTextField.isFirstResponder else { return }
    update(with: currencyPresentation)
  }
}
