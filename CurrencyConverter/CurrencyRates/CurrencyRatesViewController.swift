//
// Created by Bruno Pinheiro on 2018-11-10.
//

import Foundation
import UIKit
import Business

final class CurrencyRatesViewController: UIViewController {
  @IBOutlet var currenciesTableView: UITableView!
  @IBOutlet var activityIndicator: UIActivityIndicatorView!

  var viewModel: CurrencyRatesViewModel!

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    showActivityIndicator()
    viewModel.updateCurrencyRates { [weak self] _ in
      guard let self = self else { return }
      DispatchQueue.main.async {
        self.hideActivityIndicator()
        self.currenciesTableView.reloadData()
        self.viewModel.startPollingCurrencyData()
      }
    }
  }

  private func showActivityIndicator() {
    activityIndicator.isHidden = false
    activityIndicator.startAnimating()
  }

  private func hideActivityIndicator() {
    activityIndicator.isHidden = true
    activityIndicator.stopAnimating()
  }

  private func activateFirsCurrencyCell() {
    let firstIndexPath = IndexPath(row: 0, section: 0)
    guard let cell = currenciesTableView.cellForRow(at: firstIndexPath) else { return }
    cell.becomeFirstResponder()
  }

  override func viewDidDisappear(_ animated: Bool) {
    viewModel.stopPollingCurrencyData()
    super.viewDidDisappear(animated)
  }
}

extension CurrencyRatesViewController: UITableViewDataSource {
  struct Constants {
    static let currencyCellReuseIdentifier = "CurrencyTableCell"
  }

  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.currencyRatesCount
  }

  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.currencyCellReuseIdentifier, for: indexPath) as? CurrencyTableCell else {
      fatalError("Failed to dequeue instance of '\(String(describing: CurrencyTableCell.self))' with identifier '\(Constants.currencyCellReuseIdentifier)'")
    }

    cell.currencyData = viewModel.currency(at: indexPath)
    cell.delegate = self

    return cell
  }
}

extension CurrencyRatesViewController: UITableViewDelegate {
  public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
  }

  public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    viewModel.setBase(currencyAt: indexPath)
    let firstIndexPath = IndexPath(row: 0, section: 0)
    tableView.performBatchUpdates({
      tableView.moveRow(at: indexPath, to: firstIndexPath)
      tableView.scrollToRow(at: firstIndexPath, at: .top, animated: false)
    }, completion: { _ in
      self.activateFirsCurrencyCell()
    })
  }
}

extension CurrencyRatesViewController: CurrencyCellDelegate {
  func currencyCell(_ cell: CurrencyTableCell, didChangeValueTo valueString: String?) {
    viewModel.updateAmount(withValueString: valueString)
  }
}
