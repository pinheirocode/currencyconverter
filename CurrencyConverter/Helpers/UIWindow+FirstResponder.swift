//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation
import UIKit

extension UIView {
  var firstResponder: UIResponder? {
    guard !isFirstResponder else { return self }

    for subview in subviews {
      if let firstResponder = subview.firstResponder {
        return firstResponder
      }
    }

    return nil
  }
}
