//
// Created by Bruno Pinheiro on 2018-11-11.
//

import Foundation
import Model

class CurrencyAPIInfo: APIInfo {
  var currencyRatesUrl: String = "https://revolut.duckdns.org/latest"
}
